# Domain Adaptation for Person Re-identification on New Unlabeled Data

If you find this code useful, we kindly resquest to cite our paper below:

Pereira, T. and de Campos, T. <BR />
Domain Adaptation for Person Re-identification on New Unlabeled Data<BR />
In Proceedings of the 15th International Joint Conference on Computer Vision, Imaging and Computer Graphics Theory and Applications (VISIGRAPP 2020) - Volume 4: VISAPP, pages 695-703<BR />
*Winner of the best student paper award*<BR />
[ [pdf](http://www.insticc.org/node/TechnicalProgram/visigrapp/presentationDetails/89736) | [bibtex](https://gitlab.com/UnBVision/domain-adaptation-for-person-re-identification-on-new-unlabeled-data/-/blob/master/paper.bib) | [Presentation](https://gitlab.com/UnBVision/domain-adaptation-for-person-re-identification-on-new-unlabeled-data/-/blob/master/Presentation.pdf)]

## Contents

1. [Requirements](#requirements)
2. [Direct Transfer](#direct-Transfer)
3. [CycleGAN](#cyclegan)
4. [Pseudo Labels](#pseudo-labels)
5. [Contact](#contact)

## Requirements

The person re-identification code was initially forked from Open-ReID, so please check their [repository](https://github.com/Cysu/open-reid) and [documentation](https://cysu.github.io/open-reid/index.html#) for more informations about how to use the re-id codes.

The cycleGAN code was forked from PyTorch-GAN, so please check their [repository](https://github.com/eriklindernoren/PyTorch-GAN) for more informations.

## Direct Transfer

To run the direct transfer experiments you will use the triplet_loss.py code from Open-Reid. The following commands are examples of training and evaluating the results:

### Training

``` bash 
python examples/triplet_loss.py -d market1501 -a resnet50 --logs-dir logs/triplet-loss/directTransfer_market1501
```

### Evaluating
``` bash 
python examples/triplet_loss.py -d cuhk03 -a resnet50 --resume logs/triplet-loss/directTransfer_market1501/model_best.pth.tar --evaluate

python examples/triplet_loss.py -d viper -a resnet50 --resume logs/triplet-loss/directTransfer_market1501/model_best.pth.tar --evaluate
```

## CycleGAN

To run the cycleGAN experiments, you first need to train the cycleGAN, then run its generator to create the intermediate dataset.

### Taining the cycleGAN

The cycleGAN training is done using the PyTorch-GAN cycleGAN implementation. It was noticed that using less images from each dataset and running the training for more epochs worked better. So, first we select 1000 images from each camera from each domain and create the dataset for the cycleGAN training, using the following code:

``` bash
python codes/cycleGAN_data.py -d1p .../data/cuhk03/images/ -d1n cuhk -d2p .../data/market1501/images/ -d2n market --target-path .
```

Then, we train the cycleGAN:

``` bash
cd cyclegan/
python cyclegan.py --dataset_name cuhk2market --dataset_path .../data/cuhk2market/
```

### Create the intermediate dataset

After the cycleGAN training is done, we will use it to create the intermediate dataset with the following command:

``` bash
python codes/create_GAN_dataset.py --source-dataset-name cuhk03 -
-target-dataset-name market1501 --base-dataset-path .../data/ --generator-path ./cyclegan/saved_models/market1501_to_cuhk03/G_BA_59000.pth
```

### Fine tune the direct transfer CNN using the GAN intermediate dataset

``` bash
python examples/triplet_loss.py -d GAN_cuhk03_to_market1501 -a resnet50 --resume logs/triplet-loss/directTransfer_cuhk03/model_best.pth.tar --logs-dir logs/triplet-loss/cycleGAN_cuhk03_to_market1501 -b 8 --batch-scheduler --tl-epochs 100
```

## Pseudo Labels

### Extracting the features

``` bash
python codes/extract_features.py --nn-dir logs/triplet-loss/cycleGAN_cuhk03_to_market1501/ -d .../market1501/images/
```

### k-means

``` bash
python codes/kmeans.py --csv-file logs/triplet-loss/cycleGAN_cuhk03_to_market1501/features/market1501_features.csv 
```

### Create pseudo label dataset

``` bash
python codes/create_new_dataset.py --csv-file logs/triplet-loss/cycleGAN_cuhk03_to_market1501/groups/market1501_groups.csv --base-dataset-path .../data/

python codes/create_jsons.py --path .../data/market1501_modified/images/ --destiny .../data/market1501_modified/ -d market1501
```

### Train using pseudo labels

``` bash
python examples/triplet_loss.py --resume logs/triplet-loss/cycleGAN_cuhk03_to_market1501/model_best.pth.tar -d market1501_modified --logs-dir logs/triplet-loss/PseudoLabels_cuhk03_to_market1501 --tl-epochs 100 -b 8 --batch-scheduler
```

## Contact

If there is any problems or doubt, please contact me via email at tiagodecarvalhopereira@gmail.com