from __future__ import print_function, absolute_import
import argparse
import os.path as osp

import shutil
import os
import numpy as np
import sys
import torch
from torch import nn
from torch.backends import cudnn
from torch.utils.data import DataLoader

from reid import datasets
from reid import models
from reid.dist_metric import DistanceMetric
from reid.loss import TripletLoss
from reid.trainers import Trainer
from reid.evaluators import Evaluator
from reid.utils.data import transforms as T
from reid.utils.data.preprocessor import Preprocessor
from reid.utils.data.sampler import RandomIdentitySampler
from reid.utils.logging import Logger
from reid.utils.serialization import load_checkpoint, save_checkpoint

import multiprocessing

def get_data(name, split_id, data_dir, height, width, batch_size, num_instances,
             workers, combine_trainval):
    root = osp.join(data_dir, name)

    dataset = datasets.create(name, root, split_id=split_id)

    normalizer = T.Normalize(mean=[0.485, 0.456, 0.406],
                             std=[0.229, 0.224, 0.225])

    train_set = dataset.trainval if combine_trainval else dataset.train
    num_classes = (dataset.num_trainval_ids if combine_trainval
                   else dataset.num_train_ids)

    train_transformer = T.Compose([
        T.Resize((height, width)),
        T.RandomHorizontalFlip(),
        T.ToTensor(),
        normalizer,
    ])

    test_transformer = T.Compose([
        T.RectScale(height, width),
        T.ToTensor(),
        normalizer,
    ])


    train_loader = DataLoader(
        Preprocessor(train_set, root=dataset.images_dir,
                     transform=train_transformer),
        batch_size=batch_size, num_workers=workers,
        sampler=RandomIdentitySampler(train_set, num_instances),
        pin_memory=True, drop_last=True)

    val_loader = DataLoader(
        Preprocessor(dataset.val, root=dataset.images_dir,
                     transform=test_transformer),
        batch_size=batch_size, num_workers=workers,
        shuffle=False, pin_memory=True)

    test_loader = DataLoader(
        Preprocessor(list(set(dataset.query) | set(dataset.gallery)),
                     root=dataset.images_dir, transform=test_transformer),
        batch_size=batch_size, num_workers=workers,
        shuffle=False, pin_memory=True)

    return dataset, num_classes, train_loader, val_loader, test_loader

def batch_sched(batch, max_batch):
    if batch + 8 < max_batch:
        print("[INFO] Aumentado o batch de {} para {}".format(batch, batch + 8))
        return batch + 8
    else:
        print("[INFO] Aumentado o batch de {} para {}".format(batch, max_batch))
        return max_batch

def main(args):
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    cudnn.benchmark = True

    # Redirect print to both console and log file
    if not args.evaluate:
        sys.stdout = Logger(osp.join(args.logs_dir, 'log.txt'))

    # Create data loaders
    assert args.num_instances > 1, "num_instances should be greater than 1"
    assert args.batch_size % args.num_instances == 0, \
        'num_instances should divide batch_size'
    if args.height is None or args.width is None:
        args.height, args.width = (144, 56) if args.arch == 'inception' else \
                                  (256, 128)
    dataset, num_classes, train_loader, val_loader, test_loader = \
        get_data(args.dataset, args.split, args.data_dir, args.height,
                 args.width, args.batch_size, args.num_instances, args.workers,
                 args.combine_trainval)

    batch_size = args.batch_size

    # Create model
    # Hacking here to let the classifier be the last feature embedding layer
    # Net structure: avgpool -> FC(1024) -> FC(args.features)
    model = models.create(args.arch, num_features=1024,
                          dropout=args.dropout, num_classes=args.features)

    # Load from checkpoint
    start_epoch = best_top1 = 0
    if args.resume:
        checkpoint = load_checkpoint(args.resume)
        model.load_state_dict(checkpoint['state_dict'], strict=False)
        start_epoch = checkpoint['epoch']
        best_top1 = checkpoint['best_top1']
        if args.tl_epochs != -1 and not args.same_task:
            best_top1 = 0.0
            print("ZEROU O BEST TOP 1 pq ele foi adquirido a partir do treino em outro dataset")
        print("=> Start epoch {}  best top1 {:.1%}"
              .format(start_epoch, best_top1))
    model = nn.DataParallel(model).cuda()

    # Distance metric
    metric = DistanceMetric(algorithm=args.dist_metric)

    # Evaluator
    evaluator = Evaluator(model)
    if args.evaluate:
        metric.train(model, train_loader)
        print("Validation:")
        evaluator.evaluate(val_loader, dataset.val, dataset.val, metric)
        print("Test:")
        evaluator.evaluate(test_loader, dataset.query, dataset.gallery, metric)
        return

    # Criterion
    criterion = TripletLoss(margin=args.margin).cuda()

    # Optimizer
    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr,
                                 weight_decay=args.weight_decay)

    #opt = torchcontrib.optim.SWA(base_opt, swa_start=10, swa_freq=5, swa_lr=0.05)

    # Trainer
    trainer = Trainer(model, criterion)

    if args.tl_epochs > 0:
        start_of_weight_decay = 25
    else:
        start_of_weight_decay = 100

    lr = args.lr

    # Schedule learning rate
    def adjust_lr(epoch):
        if epoch > start_of_weight_decay:
            for g in optimizer.param_groups:
                g['lr'] = (g['lr'] * (0.001 ** ((epoch - start_of_weight_decay) / 50.0))) * g.get('lr_mult', 1)

    #Se for passado um parametro de epochs de tl, arrumamos os parametros para esse treinamento
    if args.tl_epochs != -1:
        print("\n\n\n\n\n Foi definida uma transferencia de aprendizado por {} epochs\n"
            "O treinamento anterior tinha parado em {} epochs e esse estava planejado para {} epochs\n"
            "Agora vamos ajustar o planejamento para {} epochs qeu vai nos dar {} epochs de transfer learning.".format(
                args.tl_epochs, start_epoch, args.epochs, start_epoch + args.tl_epochs, args.tl_epochs
            ))
        
        args.epochs = start_epoch + args.tl_epochs
        print("---------------------------------------------------------------------------\n"
            "Ajustamos os epochs totais para {} epochs\n\n\n\n\n".format(args.epochs))

        if not args.resume:
            raise ImportError("Nao pode fazer esse tipo de transfer learning sem uma rede pre-treinada")

        if not os.path.isfile(os.path.join(args.logs_dir, 'model_best.pth.tar')):
            print("Copiou o arquivo de onde resumia pq nao tinha um best aqui")
            shutil.copy(args.resume, os.path.join(args.logs_dir, 'model_best.pth.tar'))
        

    train_loss_hist = []
    valid_best1 = []
    train_acc_hist = []

    # Start training
    for epoch in range(start_epoch, args.epochs):
        if args.tl_epochs != -1:
            adjust_lr(epoch - start_epoch)
        else:
            adjust_lr(epoch)
            
        loss = trainer.train(epoch, train_loader, optimizer, train_loss_hist, train_acc_hist)

        if loss < 0.4 and batch_size < args.max_batch_scheduler and args.batch_scheduler:
            batch_size = batch_sched(batch_size, args.max_batch_scheduler)
            dataset, num_classes, train_loader, val_loader, test_loader = \
                get_data(args.dataset, args.split, args.data_dir, args.height,
                        args.width, batch_size, args.num_instances, args.workers,
                        args.combine_trainval)
            for g in optimizer.param_groups:
                print("[INFO] Reduzindo a lr de {} para {}".format(g['lr'], g['lr'] * 0.8))
                g['lr'] = g['lr'] * 0.8
                


        if epoch < args.start_save:
            continue
        top1 = evaluator.evaluate(val_loader, dataset.val, dataset.val)

        valid_best1.append(top1 * 100)

        is_best = top1 > best_top1
        best_top1 = max(top1, best_top1)
        save_checkpoint({
            'state_dict': model.module.state_dict(),
            'epoch': epoch + 1,
            'best_top1': best_top1,
        }, is_best, fpath=osp.join(args.logs_dir, 'checkpoint.pth.tar'))

        print('\n * Finished epoch {:3d}  top1: {:5.1%}  best: {:5.1%}{}\n'.
              format(epoch, top1, best_top1, ' *' if is_best else ''))

        if args.add_jitter > 0 and (epoch - start_epoch) % args.add_jitter == 0:
            print("[INFO] Adding jitter to model params")
            with torch.no_grad():
                for param in model.parameters():
                    param.add_(torch.randn(param.size()).cuda() * args.lr * 1000)


    # Final test
    print('Test with best model:')
    checkpoint = load_checkpoint(osp.join(args.logs_dir, 'model_best.pth.tar'))
    model.module.load_state_dict(checkpoint['state_dict'])
    metric.train(model, train_loader)
    evaluator.evaluate(test_loader, dataset.query, dataset.gallery, metric)


    if args.show_fig:
        import matplotlib.pyplot as plt
        epochs_list = list(range(1, args.epochs + 1 - start_epoch))

        #Imprime o historico do erro
        plt.figure(figsize=(5,3))
        plt.plot(epochs_list, train_loss_hist)
        plt.title('validation loss')
        plt.xlabel('epochs')
        plt.ylabel('Loss')

        #Imprime o historico do erro
        plt.figure(figsize=(5,3))
        #plt.plot(epochs_list, loss, label = "validacao", color='red')
        plt.plot(epochs_list, valid_best1)
        plt.title('validation best 1 acc')
        plt.xlabel('epochs')
        plt.ylabel('acc top 1')

        #Imprime o historico do erro
        plt.figure(figsize=(5,3))
        #plt.plot(epochs_list, loss, label = "validacao", color='red')
        plt.plot(epochs_list, train_acc_hist)
        plt.title('train acc')
        plt.xlabel('epochs')
        plt.ylabel('acc')

        plt.show()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Triplet loss classification")
    # data
    parser.add_argument('-d', '--dataset', type=str, default='cuhk03',
                        choices=datasets.names())
    parser.add_argument('-b', '--batch-size', type=int, default=256)
    parser.add_argument('-j', '--workers', type=int, default=4)
    parser.add_argument('--split', type=int, default=0)
    parser.add_argument('--height', type=int,
                        help="input height, default: 256 for resnet*, "
                             "144 for inception")
    parser.add_argument('--width', type=int,
                        help="input width, default: 128 for resnet*, "
                             "56 for inception")
    parser.add_argument('--combine-trainval', action='store_true',
                        help="train and val sets together for training, "
                             "val set alone for validation")
    parser.add_argument('--num-instances', type=int, default=4,
                        help="each minibatch consist of "
                             "(batch_size // num_instances) identities, and "
                             "each identity has num_instances instances, "
                             "default: 4")
    # model
    parser.add_argument('-a', '--arch', type=str, default='resnet50',
                        choices=models.names())
    parser.add_argument('--features', type=int, default=128)
    parser.add_argument('--dropout', type=float, default=0)
    # loss
    parser.add_argument('--margin', type=float, default=0.5,
                        help="margin of the triplet loss, default: 0.5")
    # optimizer
    parser.add_argument('--lr', type=float, default=0.0002,
                        help="learning rate of all parameters")
    parser.add_argument('--weight-decay', type=float, default=5e-4)
    # training configs
    parser.add_argument('--resume', type=str, default='', metavar='PATH')
    parser.add_argument('--evaluate', action='store_true',
                        help="evaluation only")
    parser.add_argument('--epochs', type=int, default=150)
    parser.add_argument('--start_save', type=int, default=0,
                        help="start saving checkpoints after specific epoch")
    parser.add_argument('--seed', type=int, default=1)
    parser.add_argument('--print-freq', type=int, default=1)
    # metric learning
    parser.add_argument('--dist-metric', type=str, default='euclidean',
                        choices=['euclidean', 'kissme'])
    # misc
    working_dir = osp.dirname(osp.abspath(__file__))
    parser.add_argument('--data-dir', type=str, metavar='PATH',
                        default=osp.join(working_dir, 'data'))
    parser.add_argument('--logs-dir', type=str, metavar='PATH',
                        default=osp.join(working_dir, 'logs'))
    parser.add_argument('--tl-epochs', type=int, default=-1,
                        help="Number of transfer learning epochs to use")
    parser.add_argument('--add-jitter', type=int, default=-1,
                        help="Add jitter to the weights each x epochs")    
    parser.add_argument('--batch-scheduler', action = 'store_true')
    parser.add_argument('--max-batch-scheduler', type = int , default = 64)
    parser.add_argument('--show-fig', action='store_true')
    parser.add_argument('--same-task', action='store_true')
    args = parser.parse_args()

    
    main(args)
