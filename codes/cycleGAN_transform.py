import torch.nn as nn
import torch.nn.functional as F
import torch
from torch.autograd import Variable
import random

class CycleGAN_transformer(object):
    """Crops the given PIL Image at the center.

    Args:
        size (sequence or int): Desired output size of the crop. If size is an
            int instead of sequence like (h, w), a square crop (size, size) is
            made.
    """

    def __init__(self, img_height, img_width, state_path, p = 0.2, channels = 3, n_residual_blocks = 9):
        self.prob = p
        self.cuda = torch.cuda.is_available()

        print("[INFO] cuda na transformacao: ", self.cuda)

        if self.cuda:
            self.Tensor = torch.cuda.FloatTensor
        else:
            self.Tensor = torch.Tensor

        input_shape = (channels, img_height, img_width)

        # Initialize generator and discriminator
        self.G = GeneratorResNet(input_shape, n_residual_blocks)        

        if self.cuda:
            self.G = self.G.cuda()            

        self.G.load_state_dict(torch.load(state_path))     

    def __call__(self, img):
        """
        Args:
            img (PIL Image): Image to be cropped.

        Returns:
            PIL Image: Cropped image.
        """
        if random.random() > self.prob:
            print("[INFO] GAN transform, isso nao deveria estar acontencendo")
            return img

        self.G.eval()

        img = img.unsqueeze(0)
        real_A = Variable(img.type(self.Tensor))
        
        fake_B = self.G(real_A)
        fake_B = fake_B.squeeze(0)

        fake_B = fake_B.to('cpu')
        fake_B.detach_()

        return fake_B

    def __repr__(self):
        return "CycleGAN"

##############################
#           RESNET
##############################


class ResidualBlock(nn.Module):
    def __init__(self, in_features):
        super(ResidualBlock, self).__init__()

        self.block = nn.Sequential(
            nn.ReflectionPad2d(1),
            nn.Conv2d(in_features, in_features, 3),
            nn.InstanceNorm2d(in_features),
            nn.ReLU(inplace=True),
            nn.ReflectionPad2d(1),
            nn.Conv2d(in_features, in_features, 3),
            nn.InstanceNorm2d(in_features),
        )

    def forward(self, x):
        return x + self.block(x)


class GeneratorResNet(nn.Module):
    def __init__(self, input_shape, num_residual_blocks):
        super(GeneratorResNet, self).__init__()

        channels = input_shape[0]

        # Initial convolution block
        out_features = 64
        model = [
            nn.ReflectionPad2d(channels),
            nn.Conv2d(channels, out_features, 7),
            nn.InstanceNorm2d(out_features),
            nn.ReLU(inplace=True),
        ]
        in_features = out_features

        # Downsampling
        for _ in range(2):
            out_features *= 2
            model += [
                nn.Conv2d(in_features, out_features, 3, stride=2, padding=1),
                nn.InstanceNorm2d(out_features),
                nn.ReLU(inplace=True),
            ]
            in_features = out_features

        # Residual blocks
        for _ in range(num_residual_blocks):
            model += [ResidualBlock(out_features)]

        # Upsampling
        for _ in range(2):
            out_features //= 2
            model += [
                nn.Upsample(scale_factor=2),
                nn.Conv2d(in_features, out_features, 3, stride=1, padding=1),
                nn.InstanceNorm2d(out_features),
                nn.ReLU(inplace=True),
            ]
            in_features = out_features

        # Output layer
        model += [nn.ReflectionPad2d(channels), nn.Conv2d(out_features, channels, 7), nn.Tanh()]

        self.model = nn.Sequential(*model)

    def forward(self, x):
        return self.model(x)

