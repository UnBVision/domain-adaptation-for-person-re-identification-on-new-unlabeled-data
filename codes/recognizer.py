from PIL import Image
import numpy as np
import sys

from reid import models
from reid.utils.serialization import load_checkpoint
from reid.feature_extraction import extract_cnn_feature
from reid.utils.data import transforms as T
from reid.evaluators import pairwise_distance

from torch import nn
from torch.backends import cudnn

cudnn.benchmark = True

class PersonRecognizer:
    def __init__(self, model_path):
        self.normalizer = T.Normalize(mean=[0.485, 0.456, 0.406],
                                    std=[0.229, 0.224, 0.225])
        self.test_transformer = T.Compose([
                T.RectScale(height=256, width=128),
                T.ToTensor(),
                self.normalizer,
            ])

        self.model = models.create('resnet50', num_features=1024,
                                dropout=0.5, num_classes=128)

        checkpoint = load_checkpoint(model_path)
        self.model.load_state_dict(checkpoint['state_dict'])
        self.model = nn.DataParallel(self.model).cuda()
        
    def extract_features_single (self, imagePath):
        self.model.eval()
        img = Image.open(imagePath).convert('RGB')
        img = self.test_transformer(img)
        img = np.expand_dims(img, axis=0)
        outputs = extract_cnn_feature(self.model, img)
        features = outputs.numpy()

        return features[0]

    def persons_distance(self, feature1, feature2):
        return np.linalg.norm(feature1 - feature2)

    def compare_persons(self, file1, file2, threshold=1.5):
        feature1 = self.extract_features_single(file1)
        feature2 = self.extract_features_single(file2)

        distance = self.persons_distance(feature1, feature2)

        if distance < threshold:
            return True
        else:
            return False
