import argparse
import os
from recognizer import PersonRecognizer
import csv
import time

def load_images(images_path):
    '''
    Create a dict with all the images 
    '''
    images_dict = {}

    for subdir, dirs, files in os.walk(images_path):
        for filename in files:
            images_dict[filename] = os.path.join(images_path, filename)

    return images_dict

def create_directory(path):
    '''
    If the directory do not exist yet, create it
    '''
    if not os.path.isdir(path):
        os.mkdir(path)

def generate_csv(args, images_dict, model, images_already_processed):
    '''
    Extract the features from each image in the dataset and write them in a CSV file
    '''
    csv_filename = args.dataset + "_features.csv"
    csv_filepath = os.path.join(args.nn_dir, "features")
    create_directory(csv_filepath)
    csv_filepath = os.path.join(csv_filepath, csv_filename)

    if args.overwrite:
        opening_type = 'w'
    else:
        opening_type = 'a'

    with open(csv_filepath, opening_type, newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')

        total_files = len(images_dict) - len(images_already_processed)
        i = 0
        initial_time_batch = time.time()
        total_batches = total_files / args.verbose
        batches_processed = 0
        batches_time = []

        #loop over the images and extract their features
        for key, value in images_dict.items():
            if key in images_already_processed:
                continue

            features = model.extract_features_single(value)
            features = list(map(str, features))
            writer.writerow([str(key)] + features)

            i += 1
            if i % args.verbose == 0 or i == total_files:
                batches_processed += 1
                batch_processing_time = time.time() - initial_time_batch
                initial_time_batch = time.time()

                batches_time.append(batch_processing_time)
                avg_time = sum(batches_time) / len(batches_time)

                time_remaining = (total_batches - batches_processed) * avg_time

                print("Processados: [{}]/[{}]\ttime to proccess this batch: {}\testimated time remaining: {}".format(
                    i, total_files, convert_seconds_to_time(batch_processing_time), convert_seconds_to_time(time_remaining)))

def convert_seconds_to_time(tempo):
    '''
    funcao auxiliar
    '''
    hora = int(tempo // 3600)
    tempo -= (hora * 3600)
    minuto = int(tempo // 60)
    tempo -= (minuto * 60)
    segundo = int(tempo)

    return "{:02d}:{:02d}:{:02d}".format(hora, minuto, segundo)

def read_csv(args):
    '''
    Se ja existir um arquivo csv, le ele para ver quais imagens ja foram analisadas
    '''
    csv_filename = args.dataset + "_features.csv"
    csv_filepath = os.path.join(args.nn_dir, csv_filename)

    images_already_processed = []

    if not os.path.isfile(csv_filepath):
        print("O arquivo {} ainda nao existe entao comecaremos um novo do zero".format(csv_filename))

    else:
        with open(csv_filepath, newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for row in reader:
                images_already_processed.append(row[0])

    return images_already_processed

def main(args):
    '''
    Main function
    '''
    if not args.overwrite:
        images_already_processed = read_csv(args)
    else:
        images_already_processed = []

    model_path = os.path.join(args.nn_dir, "model_best.pth.tar")
    images_path = args.dataset

    model = PersonRecognizer(model_path)

    images_dict = load_images(images_path)

    generate_csv(args, images_dict, model, images_already_processed)

if __name__ == "__main__":
    ap = argparse.ArgumentParser(description = "Feature extractor")

    ap.add_argument("--nn-dir", type = str, required = True,
                    help = "path to the directory where is stored the neural network checkpoint")
    ap.add_argument("-d", "--dataset", type = str, required = True,
                    help = "path to the directory where is stored the dataset")
    ap.add_argument("-v", "--verbose", type = int, default = 100,
                    help = "print the progress")
    ap.add_argument("--overwrite", action = "store_true",
                    help = "if active it will overwrite the existent CSV file")

    args = ap.parse_args()

    main(args)