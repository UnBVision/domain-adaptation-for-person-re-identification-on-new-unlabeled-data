from __future__ import print_function, absolute_import
import argparse
import os.path as osp

import shutil
import os
import numpy as np
import sys
import torch
from torch import nn
from torch.backends import cudnn
from torch.utils.data import DataLoader

from reid import datasets
from reid import models
from reid.dist_metric import DistanceMetric
from reid.loss import TripletLoss
from reid.trainers import Trainer
from reid.evaluators import Evaluator
from reid.utils.data import transforms as T
from reid.utils.data.preprocessor import Preprocessor
from reid.utils.data.sampler import RandomIdentitySampler
from reid.utils.logging import Logger
from reid.utils.serialization import load_checkpoint, save_checkpoint

from codes.cycleGAN_transform import CycleGAN_transformer

import multiprocessing
from torchvision.utils import save_image, make_grid
from PIL import Image
import shutil
import time
import random

parser = argparse.ArgumentParser()
parser.add_argument('--source-dataset-name', type=str, help="name of the source dataset")
parser.add_argument('--target-dataset-name', type=str, help="name of the target dataset")
parser.add_argument('--base-dataset-path', type=str, help="base path for datasets folder")
parser.add_argument('--generator-path', type=str, help="path for the cycleGAN generator path")

args = parser.parse_args()

source_dataset = args.source_dataset_name
target_dataset = args.target_dataset_name

datasets_path = args.base_dataset_path
new_dataset_name = "GAN_{}_to_{}".format(source_dataset, target_dataset)
new_dataset_path = os.path.join(datasets_path, new_dataset_name)
new_dataset_images_path = os.path.join(new_dataset_path, "images")

original_dataset_path = os.path.join(datasets_path, source_dataset)
original_dataset_images_path = os.path.join(original_dataset_path, 'images')

if not(os.path.isdir(new_dataset_path)):
    os.mkdir(new_dataset_path)

if not(os.path.isdir(new_dataset_images_path)):
    os.mkdir(new_dataset_images_path)

shutil.copy(os.path.join(original_dataset_path, 'meta.json'), os.path.join(new_dataset_path, 'meta.json'))
shutil.copy(os.path.join(original_dataset_path, 'splits.json'), os.path.join(new_dataset_path, 'splits.json'))

height = 256
width = 128

normalizer = T.Normalize(mean=[0.485, 0.456, 0.406],
                            std=[0.229, 0.224, 0.225])

train_transformer = T.Compose([
            T.Resize((height, width)),
            T.RandomHorizontalFlip(),
            T.ToTensor(),
            normalizer,
            CycleGAN_transformer(height, width, args.generator_path, p=1.0),
        ])

verbose = 500

for root, _, files in os.walk(original_dataset_images_path):
    initial_time = time.time()
    random.shuffle(files)
    for i, file in enumerate(files):
        new_image_name = os.path.join(new_dataset_images_path, file)

        if os.path.isfile(new_image_name):
            continue

        real_img = Image.open(os.path.join(root, file)).convert('RGB')

        img = train_transformer(real_img)

        save_image(img, "{}".format(new_image_name), normalize=True)

        if i % verbose == 0 or i == 0:
            print("[INFO] Transformed {}/{} images.\tLast {} images took {:.2f}s to transform".format(i, len(files), verbose, time.time() - initial_time))
            initial_time = time.time()
