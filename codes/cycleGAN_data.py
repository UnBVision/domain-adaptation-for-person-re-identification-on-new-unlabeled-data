import os 
import shutil
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-d1p', '--dataset1-path', type=str, help="path for dataset folder")
parser.add_argument('-d1n', '--dataset1-name', type=str, help="name of dataset")
parser.add_argument('-d2p', '--dataset2-path', type=str, default='market1501', help="path for dataset folder")
parser.add_argument('-d2n', '--dataset2-name', type=str, help="name of dataset")
parser.add_argument('--target-path', type=str, help="path for target dataset folder")
parser.add_argument('--split', type=float, default=0.8)

args = parser.parse_args()

dataset_1 = args.dataset1_name
dataset_2 = args.dataset2_name

datasets_paths = {
    dataset_1: args.dataset1_path,
    dataset_2: args.dataset2_path
}

train_test_split = args.split

dataset_name = dataset_1 + '_to_' + dataset_2

dataset_1_source_path = datasets_paths[dataset_1]
dataset_2_source_path = datasets_paths[dataset_2]

target_path = args.target_path

dataset_target_path = os.path.join(target_path, dataset_name)

def create_dir(dirname):
    if not os.path.isdir(dirname):
        os.mkdir(dirname)

create_dir(dataset_target_path)

dataset_target_path_train = os.path.join(dataset_target_path, 'train') 
dataset_target_path_test = os.path.join(dataset_target_path, 'test')

create_dir(dataset_target_path_test)
create_dir(dataset_target_path_train)

dataset_target_path_train_data_1 = os.path.join(dataset_target_path_train, 'A')
dataset_target_path_train_data_2 = os.path.join(dataset_target_path_train, 'B')
dataset_target_path_test_data_1 = os.path.join(dataset_target_path_test, 'A')
dataset_target_path_test_data_2 = os.path.join(dataset_target_path_test, 'B')

create_dir(dataset_target_path_train_data_1)
create_dir(dataset_target_path_train_data_2)
create_dir(dataset_target_path_test_data_1)
create_dir(dataset_target_path_test_data_2)

def copy_images(data_source, data_train_target, data_test_target, train_test_split, print_interval = 500):
    images_copied = {
        0: 0,
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,                
    }
    for root, _, files in os.walk(data_source):
        num_samples_train = int(train_test_split * len(files))

        print("[INFO] copying {} images to train".format(num_samples_train))

        for i, name in enumerate(files[:num_samples_train]):
            name_without_png = name.split(".")[0]
            _, n_cam, _ = name_without_png.split("_")
            images_copied[int(n_cam)] += 1

            if images_copied[int(n_cam)] >= 1000:
                continue

            source_path = os.path.join(root, name)
            target_path = os.path.join(data_train_target, name)

            shutil.copy(source_path, target_path)
            if i % print_interval == 0:
                print("[Stats] Already copied {}\{}".format(i, num_samples_train))

        print("[INFO] copying {} images to test".format(len(files) - num_samples_train))

        for i, name in enumerate(files[num_samples_train:]):
            source_path = os.path.join(root, name)
            target_path = os.path.join(data_test_target, name)

            if i == 6000:
                break

            shutil.copy(source_path, target_path)
            if i % print_interval == 0:
                print("[Stats] Already copied {}\{}".format(i, len(files) - num_samples_train))


copy_images(dataset_1_source_path, dataset_target_path_train_data_1, dataset_target_path_test_data_1, train_test_split)
copy_images(dataset_2_source_path, dataset_target_path_train_data_2, dataset_target_path_test_data_2, train_test_split)