import argparse
import os
import csv
import time
import shutil

def generate_csv(args, images_dict, model, images_already_processed):
    '''
    Extract the features from each image in the dataset and write them in a CSV file
    '''
    csv_filename = args.dataset + "_features.csv"
    csv_filepath = os.path.join(args.nn_dir, csv_filename)

    if args.overwrite:
        opening_type = 'w'
    else:
        opening_type = 'a'

    with open(csv_filepath, opening_type, newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')

        total_files = len(images_dict) - len(images_already_processed)
        i = 0
        initial_time_batch = time.time()
        total_batches = total_files / args.verbose
        batches_processed = 0
        batches_time = []

        #loop over the images and extract their features
        for key, value in images_dict.items():
            if key in images_already_processed:
                continue

            features = model.extract_features_single(value)
            features = list(map(str, features))
            writer.writerow([str(key)] + features)

            i += 1
            if i % args.verbose == 0 or i == total_files:
                batches_processed += 1
                batch_processing_time = time.time() - initial_time_batch
                initial_time_batch = time.time()

                batches_time.append(batch_processing_time)
                avg_time = sum(batches_time) / len(batches_time)

                time_remaining = (total_batches - batches_processed) * avg_time

                print("Processados: [{}]/[{}]\ttime to proccess this batch: {}\testimated time remaining: {}".format(
                    i, total_files, convert_seconds_to_time(batch_processing_time), convert_seconds_to_time(time_remaining)))

def convert_seconds_to_time(tempo):
    '''
    funcao auxiliar
    '''
    hora = int(tempo // 3600)
    tempo -= (hora * 3600)
    minuto = int(tempo // 60)
    tempo -= (minuto * 60)
    segundo = int(tempo)

    return "{:02d}:{:02d}:{:02d}".format(hora, minuto, segundo)

def read_csv(csv_filepath):
    '''
    Le as informacoes de grupos do arquivo csv
    '''
    groups_dict = {}

    with open(csv_filepath, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            groups_dict[row[0]] = row[1:]

    return groups_dict

def identify_dataset(csv_filename):
    '''
    Funcao para identificar de qual dataset as features pertencem, retorna o numero de cameras desse dataset
    '''
    datasets = ['market1501', 'cuhk03', 'viper']

    csv_filename = csv_filename.split("/")[-1]
    csv_filename = csv_filename.split("_")[0]
    csv_filename = csv_filename.lower()
    
    for dataset in datasets:
        if dataset in csv_filename:
            print("O dataset eh o {}".format(dataset))
            return dataset + "_modified"

def create_dataset(dataset_name, base_dataset_path):
    '''
    Checha se existe o dataset, se nao existir ja cria as pastas
    '''

    #cria dataset
    new_dataset_name = os.path.join(base_dataset_path, dataset_name)
    if os.path.isdir(new_dataset_name):
        #deleta tudo que ja tem no dataset
        shutil.rmtree(new_dataset_name)
        os.mkdir(new_dataset_name)
    else:
        os.mkdir(new_dataset_name)

    #cria pasta de imagens
    new_dataset_images_folder = os.path.join(new_dataset_name, "images")
    if not os.path.isdir(new_dataset_images_folder):
        os.mkdir(new_dataset_images_folder)

    return new_dataset_name, new_dataset_images_folder

def copy_files(groups_dict, images_path, dataset, verbose, base_dataset_path):
    '''
    Funcao para copiar os arquivos do dataset original para o novo dataset
    '''
    dataset = dataset.split("_")[0] #retira o "_modified" do nome

    #path para as imagens originais
    base_dataset_path = os.path.join(base_dataset_path, dataset)
    base_dataset_path = os.path.join(base_dataset_path, 'images')

    #codigo pra calcular o tempo restante
    total_files = len(groups_dict)
    i = 0
    initial_time_batch = time.time()
    total_batches = total_files / verbose
    batches_processed = 0
    batches_time = []

    for key, data in groups_dict.items():
        for image_name in data:
            original_image_path = os.path.join(base_dataset_path, image_name)

            new_image_name = map_images(key, image_name)
            new_image_path = os.path.join(images_path, new_image_name)

            shutil.copy2(original_image_path, new_image_path)

        #codigo pra dar uma estimativa de tempo
        i += 1
        if i % verbose == 0 or i == total_files:
            batches_processed += 1
            batch_processing_time = time.time() - initial_time_batch
            initial_time_batch = time.time()

            batches_time.append(batch_processing_time)
            avg_time = sum(batches_time) / len(batches_time)

            time_remaining = (total_batches - batches_processed) * avg_time

            print("Processados: [{}]/[{}]\ttime to proccess this batch: {}\testimated time remaining: {}".format(
                i, total_files, convert_seconds_to_time(batch_processing_time), convert_seconds_to_time(time_remaining)))

def map_images(new_id, original_name):
    '''
    Funcao para mapear o nome antigo das imagens nos novos nomes
    '''
    old_id, num_cam, img_number = original_name.split("_")

    new_name = "{:08d}".format(int(new_id))
    new_name = new_name + "_" + num_cam + "_" + img_number
    
    return new_name

def main(args):
    '''
    Main function
    '''
    dataset = identify_dataset(args.csv_file)

    dataset_path, images_path = create_dataset(dataset, args.base_dataset_path)

    groups_dict = read_csv(args.csv_file)

    copy_files(groups_dict, images_path, dataset, args.verbose, args.base_dataset_path)
    

if __name__ == "__main__":
    ap = argparse.ArgumentParser(description = "Create new dataset")

    ap.add_argument("--csv-file", type = str, required = True,
                    help = "path to the csv file containing the groups")
    ap.add_argument("-v", "--verbose", type = int, default = 100,
                    help = "print the progress")
    ap.add_argument("--base-dataset-path", type= str,
                    help= "Path for the directory containing the da datasets")

    args = ap.parse_args()

    main(args)