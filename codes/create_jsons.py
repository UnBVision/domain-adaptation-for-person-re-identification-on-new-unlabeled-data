import os
import argparse
import shutil
import json
import random

def create_dict(path):
    '''
    Will receive the path to the folder with all images and create a dictionary for it in 
the following format:

    dict = {
        "1": ['00000155_05_0020.jpg', '00000155_05_0020.jpg', ..., '00000155_05_0020.jpg'],
        "2": ['00000154_04_0021.jpg', '00000154_04_0021.jpg', ..., '00000154_04_0021.jpg'],
        "3": ['00000105_03_0029.jpg', '00000105_03_0029.jpg', ..., '00000105_03_0029.jpg']
    }
    '''
    #Instantiate the dictionary
    images_info = {}

    # loop over all uuid folders in the dataset
    for _, _, files in os.walk(path):
        for filename in files:
            id, _, _ = filename.split("_")

            if int(id) not in images_info:
                images_info[int(id)] = [filename]
            else:
                images_info[int(id)].append(filename)

    return images_info

def create_meta_json(dest_path, images_info, num_cams):
    '''
    Create and save a json called meta.json in the destiny path.
    This json contains all the person identities of the dataset, which is a list in the structure of:
    
    "identities": [
        [  # the first identity, person_id = 0
            [  # camera_id = 0
                "00000000_00_0000.jpg",
                "00000000_00_0001.jpg"
            ],
            [  # camera_id = 1
                "00000000_01_0000.jpg",
                "00000000_01_0001.jpg",
                "00000000_01_0002.jpg"
            ]
        ],
        [  # the second identity, person_id = 1
            [  # camera_id = 0
                "00000001_00_0000.jpg"
            ],
            [  # camera_id = 1
                "00000001_01_0000.jpg",
                "00000001_01_0001.jpg",
            ]
        ],
        ...
    ]
    '''

    meta = {
        "identities": []
    }

    sorted_tuples = sorted(images_info.items())

    for key, data in sorted_tuples:
        #inicializa um novo dicionario para armazenar as imagens de cada camera
        key_dict = {}

        for image_name in data:
            _, cam, _ = image_name.split("_")

            if int(cam) not in key_dict:
                key_dict[int(cam)] = [image_name]
            else:
                key_dict[int(cam)].append(image_name)

        #Checa se tem imagens de todas as cameras, caso nao tenha cria um empty array
        for num_cam in range(0, num_cams):
            if num_cam not in key_dict:
                key_dict[num_cam] = []

        sorted_key_dict = sorted(key_dict.items())

        images = []
        for key_, data_ in sorted_key_dict:
            images.append(data_)

        meta['identities'].append(images)

    json_name = os.path.join(dest_path, "meta.json")

    with open(json_name, 'w') as outfile:
        json.dump(meta, outfile)

def create_split_json(dest_path, uuid_info, train_split, query_split):
    '''
    Create and save a json called splits.json in the destiny path.
    Each dataset may define multiple training / test data splits. They are listed in splits.json, where each split defines three subsets of person identities:

    {
        "trainval": [0, 1, 3, ...],  # person_ids for training and validation
        "gallery": [2, 4, 5, ...],   # for test gallery, non-overlap with trainval
        "query": [2, 4, ...],        # for test query, a subset of gallery
    }
    
    '''
    #Create an array with all ids
    all_ids = [i for i in range(0, len(uuid_info))]

    #Generate a random trainval split randomly choosing from all ids
    trainval = [ all_ids[i] for i in sorted(random.sample(range(len(all_ids)), int(train_split * len(all_ids))))]

    #Generate the gallery with all ids that wasn't inserted in the trainval split
    gallery = [all_ids[i] for i in all_ids if i not in trainval]

    #Generaty the query split with a random subset of gallery
    query = [ gallery[i] for i in sorted(random.sample(range(len(gallery)), int(query_split * len(gallery))))]
    
    splits = {
        "trainval": trainval,
        "gallery": gallery,
        "query": query
    }

    json_name = os.path.join(dest_path, "splits.json")

    with open(json_name, 'w') as outfile:
        json.dump([splits], outfile)

def get_num_cams(dataset):
    '''
    funcao para retornar o numero de cameras que cada dataset contem 
    '''
    datasets_cams = {
        'market1501': 6,
        'cuhk03': 2,
        'viper': 2
    }

    return datasets_cams[dataset]

def main(args):
    '''
    Receive the arguments from the argument parser
    '''
    images_info = create_dict(args.path)

    num_cams = get_num_cams(args.dataset)

    create_meta_json(args.destiny, images_info, num_cams)
    create_split_json(args.destiny, images_info, args.trainval_split, args.query_split)

if __name__ == "__main__":
    #Create a argument parser and parse the arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--path", type = str, help = "path to the source folder containing all images", required = True)
    parser.add_argument("--destiny", type = str, help = "Path to the folder where we will store the images folder with the renamed images, "
                        "the meta.json and the splits.json", required = True)
    parser.add_argument("-d", "--dataset", type = str, required = True, choices = ["cuhk03","market1501", "viper"],
                    help = "path to the directory where is stored the dataset")
    parser.add_argument("--trainval-split", type = float, choices = [float("{:.2f}".format(i/10)) for i in range (0, 10)],
                        help = "which percent of the data you want to use for training / validation, the rest will be"
                        " used for testing", default = 0.7)
    parser.add_argument("--query-split", type = float, choices = [float("{:.2f}".format(i/10)) for i in range (0, 10)],
                        help = "which percent of the testing data you want to use for the query", default = 0.5)
    args = parser.parse_args()

    main(args)
