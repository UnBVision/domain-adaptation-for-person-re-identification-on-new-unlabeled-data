import cv2
import csv
import argparse
import numpy as np
import os

def read_csv(csv_path, num_cams):
    '''
    Le o arquivo CSV e pega o nome das imagens e as features delas

    A saida do features_dict tem um formato:
        features_dict = {
            0: {            #cam 0
                "000000_00_00.jpg": [0.0012, -0.1224, ..., 0.3434232]  #Nome do arquivo: features
            },
            1: {            #cam 1
                "000000_01_00.jpg": [0.0012, -0.1224, ..., 0.3434232]  #Nome do arquivo: features
            },
            ...
            ...
            ...
            n: {            #cam n
                "000000_0n_00.jpg": [0.0012, -0.1224, ..., 0.3434232]  #Nome do arquivo: features
            },
        }
    '''
    features_dict = {}

    for i in range(num_cams):
        features_dict[i] = {}

    with open(csv_path, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            _, cam, _ = row[0].split('_')
            
            features_dict[int(cam)][row[0]] = list(map(float, row[1:]))

    return features_dict

def prepare_data(features_dict):
    '''
    Funcao para organizar as features da forma esperada pelo cv2 para aplicar o k means
    
    A saida vai ter um formato:
        magic_dict = {
            0: {            #cam 0
                "features": [[0.0012, -0.1224, ..., 0.3434232], [-0.09932, -0.014756, ..., 0.65346]],  #Vetor de features
                "images_name": ["000000_00_00.jpg", "000000_00_01.jpg"]                                #Vetor de filenames
            },
            1: {            #cam 1
                "000000_00_00.jpg": [0.0012, -0.1224, ..., 0.3434232]  #Nome do arquivo: features
            },
            ...
            ...
            ...
            n: {            #cam n
                "000000_00_00.jpg": [0.0012, -0.1224, ..., 0.3434232]  #Nome do arquivo: features
            },
        }

        NOTE: Os indices dos vetores de features e images name falam da mesma imagem, ou seja, a feature que esta em features[0] foi 
              extraida da imagem que esta em images_name[0]
    '''
    magic_dict = {}

    for key, data in features_dict.items():
        magic_dict[key] = {
            "features": [],
            "images_names": []    
        }

        for key_, data_ in data.items():
            magic_dict[key]['features'].append(data_)
            magic_dict[key]['images_names'].append(key_)

        magic_dict[key]['features'] = np.float32(magic_dict[key]['features'])

    return magic_dict

def identify_dataset(csv_filename):
    '''
    Funcao para identificar de qual dataset as features pertencem, retorna o numero de cameras desse dataset
    '''
    datasets_cams = {
        'market': 6,
        'cuhk': 2,
        'viper': 2
    }

    csv_filename = csv_filename.split("/")[-1]
    csv_filename = csv_filename.split("_")[0]
    csv_filename = csv_filename.lower()
    
    for key, data in datasets_cams.items():
        if key in csv_filename:
            print("O dataset eh o {} que tem {} cameras".format(key, data))
            return key, data

    raise ValueError("O arquivo csv nao apresenta o nome de nenhum dataset esperado")

def determine_k(dataset_name, input_k):
    '''
    Determina qual o k vai ser usado no k_means. Se o input de usuario do terminal for valido, utiliza ele,
    caso contrario utiliza um valor padrao para cada dataset
    '''
    if input_k != -1:
        return input_k
    else:
        datasets_k = {
            'market': 1600,
            'cuhk': 2000,
            'viper': 632
        }

        return datasets_k[dataset_name]

def apply_Kmeans(magic_dict, k):
    '''
    Funcao que aplica o Kmeans para fazer os agrupamentos por camera

    A saida tera o formato:

    kmeans_dict = {
        0: {                    #cam 0
            'centers': [
                [0.0012, -0.1224, ..., 0.3434232],
                [0.0012, -0.1224, ..., 0.3434232],
                [0.0012, -0.1224, ..., 0.3434232],
                ...,
                [0.0012, -0.1224, ..., 0.3434232]
            ],
            'ret': 8929,         #ret value
            'groups': {
                0: ["000000_00_00.jpg", "000000_00_01.jpg", "000000_00_02.jpg"],
                1: ["000001_00_00.jpg", "000001_00_01.jpg", "000001_00_02.jpg"],
                2: ["000002_00_00.jpg", "000002_00_01.jpg", "000002_00_02.jpg"],
                ...,
                n: ["00000n_00_00.jpg", "00000n_00_01.jpg", "00000n_00_02.jpg"],
            }
        }
        1: {                    #cam 1
            'centers': [
                [0.0012, -0.1224, ..., 0.3434232],
                [0.0012, -0.1224, ..., 0.3434232],
                [0.0012, -0.1224, ..., 0.3434232],
                ...,
                [0.0012, -0.1224, ..., 0.3434232]
            ],
            'ret': 8929,         #ret value
            'groups': {
                0: ["000000_01_00.jpg", "000000_01_01.jpg", "000000_01_02.jpg"],
                1: ["000001_01_00.jpg", "000001_01_01.jpg", "000001_01_02.jpg"],
                2: ["000002_01_00.jpg", "000002_01_01.jpg", "000002_01_02.jpg"],
                ...,
                n: ["00000n_01_00.jpg", "00000n_01_01.jpg", "00000n_01_02.jpg"],
            }
    }
    '''
    kmeans_dict = {}

    print("[INFO] Starting K-means")

    #faz um loop entre as cameras para aplicar um k-means em cada camera
    for key, data in magic_dict.items():
        kmeans_dict[key] = {}

        features = magic_dict[key]['features']
        images_names = magic_dict[key]['images_names']

        # define criteria, number of clusters(K) and apply kmeans()
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 1.0)
        ret, label, center = cv2.kmeans(features, k, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)

        kmeans_dict[key]['groups'] = {}

        #Faz um loop nos labels e coloca o nome das imagens no dicionario 
        for i, lab in enumerate(label):
            if int(lab[0]) not in kmeans_dict[key]['groups']:
                kmeans_dict[key]['groups'][int(lab[0])] = [images_names[i]]
            else:
                kmeans_dict[key]['groups'][int(lab[0])].append(images_names[i])
    
        kmeans_dict[key]['centers'] = center
        kmeans_dict[key]['ret'] = ret

    print("[INFO] Finished K-means")

    return kmeans_dict

def final_grouping(kmeans_dict):
    '''
    Funcao para agrupar os grupos do kmeans

    A saida tera o formato:

    final_groups = {
        0: {                                                                        #grupo 0
            'center': [1.214214214, -0.9320932, ...., 0.0024312412],                 #coordenadas do centro do grupo
            'images': [00000_00_01.jpg, 00000_01_01.jpg, ..., 00000_02_01.jpg]      #nome das imagens pertencentes ao grupo
        },
        1: {                                                                        #grupo 1
            'center': [1.214214214, -0.9320932, ...., 0.0024312412],                #coordenadas do centro do grupo
            'images': [00001_00_01.jpg, 00001_01_01.jpg, ..., 00001_02_01.jpg]      #nome das imagens pertencentes ao grupo
        }, 
        ...
        ...
        ...
        n: {                                                                        #grupo n
            'center': [1.214214214, -0.9320932, ...., 0.0024312412],                #coordenadas do centro do grupo
            'images': [0000n_00_01.jpg, 0000n_01_01.jpg, ..., 0000n_02_01.jpg]      #nome das imagens pertencentes ao grupo
        },
    }
    '''
    final_groups = {}

    print("[INFO] Starting the camera grouping proccess")

    #faz um loop no dicionario do k-means:
    for key, data_dict in kmeans_dict.items():
        #se for a primeira camera a ser analisada, apenas copia os centros e as imagens de cada grupo
        if key == 0:
            centers = kmeans_dict[key]['centers'].tolist()

            for group_key, group_array in kmeans_dict[key]['groups'].items():
                final_groups[group_key] = {
                    'center': centers[group_key],
                    'images': group_array
                }
        #se for outra camera, faz um k-nn 1 -> all (one to all) para fazer os agrupamentos
        else:
            final_centers = []              #centros que ja estao no dict de saida
            final_centers_matched = []      #boolean que indica se esse centro ja deu match pra essa camera
            final_keys = []                 #numero do grupo indexada na mesma ordem que os centros
            
            #pega as informacoes que ja estao nos grupos finais
            for final_group_key, final_group_array in final_groups.items():
                final_keys.append(final_group_key)
                final_centers.append(final_groups[final_group_key]['center'])
                final_centers_matched.append(False)

            centers = kmeans_dict[key]['centers'].tolist()

            for group_key, group_array in kmeans_dict[key]['groups'].items():
                actual_center = centers[group_key]

                distances = np.linalg.norm(np.asarray(actual_center) - np.asarray(final_centers), axis=1).tolist()
                sorted_indexes = np.argsort(distances).tolist()

                for index in sorted_indexes:
                    if final_centers_matched[index]:
                        continue
                    else:
                        final_groups[final_keys[index]]['images'] = list(set(final_groups[final_keys[index]]['images'] + group_array))
                        average_center = np.mean(np.asarray([actual_center, final_centers[index]]), axis = 0)
                        final_groups[final_keys[index]]['centers'] = average_center
                        final_centers_matched[index] = True
                        break

    print("[INFO] Finished camera grouping proccess")

    return final_groups

def create_dir(filepath):
    '''
    If the groups directory does not exist yet, creates it
    '''
    paths = filepath.split("/")

    dir_path = ''

    for path in paths[:-1]:
        dir_path = os.path.join(dir_path, path)

    if not os.path.isdir(dir_path):
        os.mkdir(dir_path)

def print_result(final_groups, csv_filename):
    '''
    funcao para escrever o arquivo csv com os grupos
    '''
    group_filename = csv_filename.replace("features", "groups", 2)
    create_dir(group_filename)

    with open(group_filename, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')

        for key, data in final_groups.items():
            writer.writerow([str(key)] + data['images'])


def main(args):
    '''
    Funcao principal 
    '''
    dataset_name, num_cams =  identify_dataset(args.csv_file)

    features_dict = read_csv(args.csv_file, num_cams)

    magic_dict = prepare_data(features_dict)

    k = determine_k(dataset_name, args.k)

    kmeans_dict = apply_Kmeans(magic_dict, k)

    final_groups = final_grouping(kmeans_dict)

    print_result(final_groups, args.csv_file)

if __name__ == '__main__':
    ap = argparse.ArgumentParser()

    ap.add_argument("--csv-file", type = str, help = "path to CSV file")
    ap.add_argument("-k", type = int, default = -1, help = "which k to use in k-means")

    args = ap.parse_args()

    main(args)